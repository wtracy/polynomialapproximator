import unittest

from approximator import *

class IATest(unittest.TestCase):
    def testEmpty(self):
        self.assertEqual(solve(10, {}), {})

    def testOne(self):
        self.assertEqual(solve(10, {0: Entry(1, 1)}), {0: 10})

    def testGoldExample(self):
        self.assertEqual(
            solve(500, {'silver': Entry(40, 1),'gold':Entry(200,1)}),
            {'silver': 6, 'gold': 1}
        )

    def testGoldRoundingExample(self):
        self.assertEqual(
                solve(500, {'silver': Entry(39, 1),'gold':Entry(200,1)}), 
                {'silver': 7, 'gold': 1}
        )

    def testZeroEntry(self):
        self.assertEqual(
                solve(500, {'silver': Entry(40, 0), 'gold':Entry(200,1)}), 
                {'silver': 0, 'gold': 2}
        )

    def testDifferentEntrys(self):
        self.assertEqual(
                solve(100, {0: Entry(1, 1), 1: Entry(1, 9)}), 
                {0: 10, 1: 90}
        )

if __name__ == '__main__':
    unittest.main()
