# Integer approximation of the solution to a linear polynomial.


class Entry:
    def __init__(self, price, weight):
        self.price = price
        self.weight = weight

def solve(total, entries):
    answers = {}

    while len(entries) > 0:
        estimates = {}
        errors = {}
        generateEstimates(entries, total, estimates, errors)
        best = findMinError(errors)

        answers[best] = int(estimates[best])
        total -= answers[best] * entries[best].price
        del entries[best]
    return answers

def generateEstimates(entries, total, estimates, errors):
    totalWeight = sum([entries[e].weight for e in entries])
    for e in entries:
        estimate = (
                total / entries[e].price * 
                entries[e].weight / totalWeight
        )
        estimates[e] = estimate
        errors[e] = abs(estimate - int(estimate))

def findMinError(errors):
    minError = -1
    best = None
    for e in errors:
        if minError == -1 or errors[e] < minError:
            best = e
            minError = errors[e]
    return best
